<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <?php get_header( hji_theme_template_base() ); ?>

    <body <?php body_class(); ?>>

        <?php do_action( 'hji_theme_body_start' ); ?>

        <!--[if lt IE 9]>
            <div class="alert alert-warning">
                <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'hji-textdomain'); ?>
            </div>
        <![endif]-->

        <div id="wrapper" class="body-wrapper">

            <?php do_action( 'hji_theme_before_navbar' ); ?>
            
            <?php get_template_part( 'templates/header-navbar' ); ?>
            
            <?php do_action( 'hji_theme_after_navbar' ); ?>

            <div class="blvd-slideshow"></div>

            <section id="primary" class="primary-wrapper container">

                <?php do_action( 'hji_theme_layout_before' ); ?>

                <div class="row">

                    <?php do_action( 'hji_theme_before_content' ); ?>

                    <div id="content" class="<?php echo hji_theme_main_class(); ?>" role="main">

                        <div class="row">
                        <div class="col-md-9 col-sm-12 push-right">
                            <div class="tab-section">
                             <ul id="hji-area-tabs" class="nav nav-tabs" role="tablist">
                              <li class="active"><a href="#area-tab-1" role="tab" data-toggle="tab">Find Your Home</a></li>                        
                              <li><a href="#area-tab-2" role="tab" data-toggle="tab">Sell Your Home</a></li>                        
                             </ul>
                             <div class="tab-content">
                              <div class="tab-pane active" id="area-tab-1">
                               <?php echo do_shortcode('[simple_qsw mls="actris"]'); ?>
                              </div>
                              <div class="tab-pane" id="area-tab-2">   
                               <?php echo do_shortcode('[home_value_form button="Get Started"]'); ?>
                              </div>
                             </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 push-left agentinfowrap">
                            <?php the_post_thumbnail( 'agent-roster-photo' ); ?>
                            <div class="agent-box">
                            <h3><?php echo $fields['first_name'] . ' ' . $fields['last_name']; ?></h3>
                            <p class="agent-position"><strong>Residential Specialist</strong></p>
                            <p>E: <a href="javascript:;" data-toggle="modal" data-target="#agent-contact-modal"><?php echo antispambot( $fields['email'] ); ?></a></p>
                            <p>P: <?php echo $fields['office_phone']; ?></p>
                            <p>A: <?php echo $office_address; ?></p>
                            <?php // BEGIN Social Media Links

                            $icons = false;
                            $soc_med = array( 'facebook', 'google_plus', 'twitter', 'linkedin', 'pinterest', 'youtube' );

                            foreach ($soc_med as $item) {
                                if ( isset( $fields[$item] ) ) {
                                    $icon_name = $item;
                                    $icons .= '<a target="_blank" class="agent-roster-soc-media" href="' . $fields[$item] . '"><img class="' . $icon_name . '" src="' . AGRO_IMAGES . 'iconset/' . $icon_name . '.png" alt="' . ucwords( str_replace('_', ' ', $icon_name) ) . ' Icon" title="' . ucwords( str_replace('_', ' ', $icon_name) ). ' Profile of ' . $fields['first_name'] . ' ' . $fields['last_name'] . '"/></a>';
                                }
                            }

                            if ($icons) {
                                echo $icons;
                            }
                            // END Social Media Links ?>        
                            </div>
                        </div>
                        </div>


                        <?php if ( is_page_template( 'template-homepage.php' ) ) : ?>

                            <?php include_once( 'template-homepage.php' ); ?>

                        <?php else : ?>

                            <?php do_action( 'hji_theme_before_content_col' ); ?>

                            <?php include hji_theme_template_path(); ?>

                            <?php do_action( 'hji_theme_after_content_col' ); ?>

                            <?php get_template_part( 'templates/cta-boxes' ); ?>

                        <?php endif; ?>

                    </div>

                    <?php do_action( 'hji_theme_before_sidebar' ); ?>
                    
                    <?php ( hji_theme_display_sidebar() ? get_sidebar( hji_theme_template_base() ) : '' ) ?>

                    <?php do_action( 'hji_theme_after_sidebar' ); ?>

                </div>

                <?php do_action( 'hji_theme_layout_after' ); ?>
            
            </section>

            <?php do_action( 'hji_theme_after_primary' ); ?>

            <section class="container">

                <?php if ( is_active_sidebar( 'blvd-homewidgets' ) ) : ?>

                    <div class="blvd-home-widgets row">
                        <?php dynamic_sidebar( 'blvd-homewidgets'); ?>
                    </div>

                <?php endif; ?>

                <?php if ( is_active_sidebar( 'blvd-hometabwidgets' ) ) : ?>

                    <div class="blvd-tabwrap">
                        <ul class="nav nav-tabs"></ul>

                        <div class="blvd-tabs-container tab-content">
                            <?php dynamic_sidebar('blvd-hometabwidgets'); ?>
                        </div>
                        
                    </div>

                <?php endif; ?>

            </section>

            <?php get_footer( hji_theme_template_base() ); ?>

        </div>

        <?php do_shortcode( 'hji_theme_body_end' ); ?>

    </body>

</html>